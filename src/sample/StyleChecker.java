package sample;

public class StyleChecker {
    public int Run() {
        Runtime rt = Runtime.getRuntime();
        int retVal = 0;
        try {
            Process process = rt.exec("notepad");
            retVal = process.waitFor();
        }
        catch(Exception e) {
            System.out.println("Error");
        }
        return retVal;
    }
}
